#!/bin/bash
# Default acpi script that takes an entry for all actions

case "$1" in
    button/sleep)
        case "$2" in
            SLPB|SBTN)
                logger 'SleepButton pressed'
		rc-service x200-my restart
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
    button/lid)
        case "$3" in
            close)
                logger 'LID closed'
                ;;
            open)
                logger 'LID opened'
		rc-service x200-my restart
                ;;
            *)
                logger "ACPI action undefined: $3"
                ;;
        esac
        ;;
esac

