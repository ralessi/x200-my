---
author:
- Robert Alessi
title: 'The x200-my package – README file'
subtitle: 'An openrc service dedicated to Thinkpads X200'
---

End of Development Notice
=========================
x200-my will no longer receive any updates.  Development has ceased.
Please refer to [my-x200 for
Debian/Devuan](http://git.robertalessi.net/my-x200) instead.

Overview
--------
x200-my is a simple openrc script primarily designed to make the
distinctive whining noise of Thinkpads X200 disappear.  But it can
also be used on other laptops.

It is written to be used on [Hyperbola
GNU/Linux-libre](https://www.hyperbola.info) but it should work on
other Arch-based + openrc distributions.

Privacy Settings
----------------
It also provides a “Privacy settings” section: if `enable_privacy` is
set to `true`, then the wired internet interface that may or may not
be connected to the internet will have its MAC address spoofed at
startup. (The wireless interfaces must also be spoofed, but this can
be handled by wpa_supplicant: see below.)  Additionally, x200-my will
delete `/etc/dhcpcd.duid`, which will make `dhcpcd` generate a new
DUID-LLT based file with an updated timestamp and the spoofed
link-layer address of the network interface that is connected to the
DHCP device at the time that the DUID is generated.

Backup
------
Finally x200-my has the ability to backup essential files or
directories—for instance to a backup disk mounted inside the dock.  Of
course, the backup process is triggered on certain conditions only:

1. The option must be enabled in the `/etc/conf.d/x200-my` file.
2. The disk must be mounted.
3. An (empty) `dobackup` file must be present in the backup directory.

Then, once these conditions are met, the backup process is triggered
whenever the service is stopped.  It additionally generates a list of
installed packages in case the disk should die and everything should
be reinstalled.

License and Disclamer
=====================
x200-my - An openrc service dedicated to Thinkpads X200
Copyright (C) 2019  Robert Alessi <alessi@robertalessi.net>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

Installation
============
1. Clone this repository locally.
2. Do `makepkg`.
3. Install the generated x200-my package as root with `pacman -U`.
4. Edit `/etc/conf.d/x200-my`.

Use and recommended configuration
---------------------------------
It is advisable to use x200-my in conjunction with `wpa_supplicant`
and `dhcpcd` only.  In addition, `wpa_gui` and `dhcpcd-ui` are also
recommended.

For this to work properly, it is important to have these two lines in
`/etc/wpa_supplicant/wpa_supplicant.conf`:

````
mac_addr=1
preassoc_mac_addr=1
````

And these settings in `/etc/dhcpcd.conf`:

````
hostname "localhost"
hostname_short
````

More information in `x200-my` initd file:
<https://git.robertalessi.net/x200-my/tree/x200-my.confd>
